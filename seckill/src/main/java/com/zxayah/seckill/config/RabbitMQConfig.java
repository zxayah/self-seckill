package com.zxayah.seckill.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 张俊琦
 * @version 1.0
 * 2023/5/10 16:30
 */
@Configuration
public class RabbitMQConfig {

    //--direct---
    private static final String QUEUE_DIRECT1 = "queue_direct01";
    private static final String QUEUE_DIRECT2 = "queue_direct02";
    private static final String EXCHANGE_DIRECT = "directExchange";
    //路由
    private static final String ROUTING_KEY01 = "queue.red";
    private static final String ROUTING_KEY02 = "queue.green";

    //-----direct-----
    /**
     * 创建/配置队列 QUEUE_DIRECT1 (queue_direct01)
     *
     * @return
     */
    @Bean
    public Queue queue_direct1() {
        return new Queue(QUEUE_DIRECT1);
    }

    /**
     * 创建/配置队列 QUEUE_DIRECT2 (queue_direct02)
     *
     * @return
     */
    @Bean
    public Queue queue_direct2() {
        return new Queue(QUEUE_DIRECT2);
    }

    /**
     * 创建/配置 交换机 EXCHANGE_DIRECT(directExchange)
     *
     * @return
     */
    @Bean
    public DirectExchange exchange_direct() {
        return new DirectExchange(EXCHANGE_DIRECT);
    }

    /**
     * 说明
     * 1. 将队列  queue_direct1()/QUEUE_DIRECT1绑定到指定的交换机exchange_direct()EXCHANGE_DIRECT(directExchange)
     * 2. 同时声明了/关联路由 ROUTING_KEY01(queue.red)
     * (1)队列 ： queue_direct1()
     * (2)交换机 :exchange_direct()
     * (3)路由 ROUTING_KEY01
     * @return
     */
    @Bean
    public Binding binding_direct1() {
        return BindingBuilder
                .bind(queue_direct1()).to(exchange_direct()).with(ROUTING_KEY01);
    }

    @Bean
    public Binding binding_direct2() {
        return BindingBuilder
                .bind(queue_direct2()).to(exchange_direct()).with(ROUTING_KEY02);

    }

}
