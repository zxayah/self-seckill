package com.zxayah.seckill.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @author 张俊琦
 * @version 1.0
 * 2023/5/10 16:38
 */
@Service
@Slf4j
public class MQReceiver {
    /**
     * 监听队列 queue_topic01
     *
     * @param msg
     */
    @RabbitListener(queues = "queue_topic01")
    public void queue_topic01(Object msg) {
        log.info("从queue_topic01 接收到消息-->" + msg);
    }

    /**
     * 监听队列 queue_topic01
     *
     * @param msg
     */
    @RabbitListener(queues = "queue_topic02")
    public void queue_topic02(Object msg) {
        log.info("从queue_topic02 接收到消息-->" + msg);
    }

}
