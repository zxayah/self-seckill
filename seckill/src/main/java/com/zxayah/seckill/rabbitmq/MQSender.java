package com.zxayah.seckill.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 张俊琦
 * @version 1.0
 * 023/5/10 16:36
 */
@Service
@Slf4j
public class MQSender {
    //装配RabbitTemplate->操作RabbitMQ
    @Resource
    private RabbitTemplate rabbitTemplate;

    //方法:发送消息到topic交换机，同时指定路由 queue.red.message
    public void sendTopic3(Object msg) {
        log.info("发送消息-->" + msg);
        rabbitTemplate.convertAndSend("topicExchange", "queue.red.message", msg);
    }

    //方法:发送消息到topic交换机，同时指定路由 green.queue.green.message
    public void sendTopic4(Object msg) {
        log.info("发送消息-->" + msg);
        rabbitTemplate.convertAndSend("topicExchange", "green.queue.green.message", msg);
    }
}
