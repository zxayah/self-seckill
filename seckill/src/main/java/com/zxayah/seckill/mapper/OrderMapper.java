package com.zxayah.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxayah.seckill.pojo.Order;

public interface OrderMapper extends BaseMapper<Order> {
    //如果有需求,可以增加新的方法..
}