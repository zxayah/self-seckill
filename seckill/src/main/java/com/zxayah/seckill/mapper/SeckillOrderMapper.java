package com.zxayah.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxayah.seckill.pojo.SeckillOrder;

public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {
}