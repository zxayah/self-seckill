package com.zxayah.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxayah.seckill.pojo.SeckillGoods;

public interface SeckillGoodsMapper
        extends BaseMapper<SeckillGoods> {
}