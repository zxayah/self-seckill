package com.zxayah.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxayah.seckill.pojo.User;

public interface UserMapper extends BaseMapper<User> {
}