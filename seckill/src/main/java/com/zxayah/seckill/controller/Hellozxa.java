package com.zxayah.seckill.controller;

import com.zxayah.seckill.pojo.User;
import com.ramostear.captcha.HappyCaptcha;
import com.ramostear.captcha.common.Fonts;
import com.ramostear.captcha.support.CaptchaStyle;
import com.ramostear.captcha.support.CaptchaType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 张俊琦
 * @version 1.0
 * 2023/5/5 11:04
 */
@Controller
public class Hellozxa {
    @Resource
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "hello")
    @ResponseBody
    public String hello(){
        redisTemplate.opsForValue().set("zxa:number", 10);
        return "zxaHello";
    }
    //生成验证码-happyCaptcha
    @RequestMapping("/zxa/captcha")
    public void happyCaptcha(HttpServletRequest request, HttpServletResponse response, User user, Long goodsId) {
        //生成验证码，并输出
        //注意，该验证码，默认就保存到session中, key是 happy-captcha
        HappyCaptcha.require(request, response)
                .style(CaptchaStyle.ANIM)               //设置展现样式为动画
                .type(CaptchaType.NUMBER)               //设置验证码内容为数字
                .length(6)                              //设置字符长度为6
                .width(220)                             //设置动画宽度为220
                .height(80)                             //设置动画高度为80
                .font(Fonts.getInstance().zhFont())     //设置汉字的字体
                .build().finish();

        System.out.println(request.getSession().getAttribute("happy-captcha"));//生成并输出验证码
    }

}
