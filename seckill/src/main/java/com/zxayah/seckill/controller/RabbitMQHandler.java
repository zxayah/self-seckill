package com.zxayah.seckill.controller;

import com.zxayah.seckill.rabbitmq.MQSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author 张俊琦
 * @version 1.0
 * 2023/5/10 20:09
 */
@Controller
public class RabbitMQHandler {
    @Resource
    private MQSender mqSender;

    @RequestMapping("/mq/topic01")
    @ResponseBody
    public void topic01() {
        mqSender.sendTopic3("hello,red");
    }

    //方法: 调用消息生产者，发送消息到交换机topicExchange
    @RequestMapping("/mq/topic02")
    @ResponseBody
    public void topic02() {
        mqSender.sendTopic4("hello,green");
    }
}
