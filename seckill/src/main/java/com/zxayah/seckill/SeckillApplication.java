package com.zxayah.seckill;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 张俊琦
 * @version 1.0
 * 2023/5/5 11:00
 */
@SpringBootApplication
@MapperScan("com.zxayah.seckill.mapper")
public class SeckillApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeckillApplication.class, args);
    }
}
