package com.zxayah.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxayah.seckill.mapper.SeckillGoodsMapper;
import com.zxayah.seckill.pojo.SeckillGoods;
import com.zxayah.seckill.service.SeckillGoodsService;
import org.springframework.stereotype.Service;

@Service
public class SeckillGoodsServiceImpl
        extends ServiceImpl<SeckillGoodsMapper, SeckillGoods>
        implements SeckillGoodsService {
}