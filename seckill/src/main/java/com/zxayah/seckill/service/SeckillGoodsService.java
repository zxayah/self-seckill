package com.zxayah.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxayah.seckill.pojo.SeckillGoods;

public interface SeckillGoodsService extends IService<SeckillGoods> {
}