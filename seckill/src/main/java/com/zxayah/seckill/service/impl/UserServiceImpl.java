package com.zxayah.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxayah.seckill.exception.GlobalException;
import com.zxayah.seckill.mapper.UserMapper;
import com.zxayah.seckill.pojo.User;
import com.zxayah.seckill.service.UserService;
import com.zxayah.seckill.util.CookieUtil;
import com.zxayah.seckill.util.MD5Util;
import com.zxayah.seckill.util.UUIDUtil;
import com.zxayah.seckill.vo.LoginVo;
import com.zxayah.seckill.vo.RespBean;
import com.zxayah.seckill.vo.RespBeanEnum;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 张俊琦
 * @version 1.0
 * 2023/5/5 13:52
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public RespBean doLogin(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response) {
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();

        /*//判断手机号/id 和密码是否为空
        if (!StringUtils.hasText(mobile) || !StringUtils.hasText(password)) {
            return RespBean.error(RespBeanEnum.LOGIN_ERROR);
        }

        ///校验手机号码是否合格
        if (!ValidatorUtil.isMobile(mobile)) {
            return RespBean.error(RespBeanEnum.MOBILE_ERROR);
        }*/

        //查询DB, 看看用户是否存在
        User user = userMapper.selectById(mobile);
        if (null == user) {//说明用户不存在
            //return RespBean.error(RespBeanEnum.LOGIN_ERROR);
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }

        //如果用户存在，则比对密码!!
        //注意，我们从loginVo取出的密码是中间密码(即客户端经过一次加密加盐处理的密码)
        if (!MD5Util.midPassToDBPass(password, user.getSlat()).equals(user.getPassword())) {
            return RespBean.error(RespBeanEnum.LOGIN_ERROR);
        }

        //System.out.println("==========");

        //给每个用户生成ticket-唯一
        String ticket = UUIDUtil.uuid();
        //将登录成功的用户保存到session
        //request.getSession().setAttribute(ticket, user);
        //为了实现分布式Session, 把登录的用户存放到Redis
        System.out.println(ticket);

        redisTemplate.opsForValue().set("user:" + ticket, user);
        //将ticket保存到cookie
        CookieUtil.setCookie(request, response, "userTicket", ticket);
        //这里我们需要返回userTicket
        System.out.println("==========");
        return RespBean.success(ticket);
    }

    @Override
    public User getUserByCookie(String userTicket, HttpServletRequest request, HttpServletResponse response) {

        if (!StringUtils.hasText(userTicket)) {
            return null;
        }

        //根据userTicket到Redis获取user
        User user = (User) redisTemplate.opsForValue().get("user:" + userTicket);
        //如果用户不为null, 就重新设置cookie,刷新，防止Cookie自动过期了, 这里是根据你的业务需要来的
        if (user != null) {
            CookieUtil.setCookie(request, response, "userTicket", userTicket);
        }

        return user;
    }
}
