package com.zxayah.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxayah.seckill.mapper.SeckillOrderMapper;
import com.zxayah.seckill.pojo.SeckillOrder;
import com.zxayah.seckill.service.SeckillOrderService;
import org.springframework.stereotype.Service;

@Service
public class SeckillOrderServiceImpl
        extends ServiceImpl<SeckillOrderMapper, SeckillOrder>
        implements SeckillOrderService {
}